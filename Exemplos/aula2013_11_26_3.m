clear
clc
%ex2:
Hz2Numerador=[1,4,6,4,1]
Hz2Denominador=[1,0,0.486,0,0.0177]
[H,w]=freqz(Hz2Numerador,Hz2Denominador)%freqz retorna a resposta em frequ�ncia. Por padr�o a amostragem � feita em 512 pontos.
subplot(1,2,1)
plot(w,abs(H)/max(abs(H)))%resposta em frequencia, em fun��o de w (omega) de 0 a Pi.
grid
freqDeAmostragem=1000%exemplo, se a frequencia de amostragem de 1000 Hertz
subplot(1,2,2)
freq=w*(freqDeAmostragem/(2*pi))
plot(freq,abs(H)/max(abs(H)))%resposta em frequencia, em fun��o da frequencia em Hz.
grid
%ou entao, freqz aceita a frequencia de amostragem em forma de parametro e
%ja retorna o w em funcao da frequencia.
% [H,freq]=freqz(Hz2Numerador,Hz2Denominador,512,freqDeAmostragem)
% plot(freq,abs(H))
% hDeN=impz(Hz2Numerador,Hz2Denominador,512)
% plot(hDeN(1:100))
% Hz=fft(hDeN)
% plot(abs(Hz))
% %ex3
% meuFiltro=fir1(6,0.5)