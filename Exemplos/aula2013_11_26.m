clear
clc
%ex1:
HzNumerador=[1,-10,-4,4]%numerador da fun��o de transfer�ncia
HzDenominador=[2,-2,-4]%denominador da fun��o de transfer�ncia
[R,P,K]=residuez(HzNumerador,HzDenominador)% espande a fun��o de transferencia em fra��es parciais, onde: K � o quociente, caso o seja uma fra��o imparcial; P s�o os polos, e R s�o os escalares que multiplicam cada termo contendo um polo.
zplane(HzNumerador,HzDenominador)%plota os polos e zeros