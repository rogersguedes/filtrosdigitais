%limpando as vari�veis e a tela{
clear
clc
%}

%gerando sinal de entrada{
numDeAmostras=[0:100]
freqDeAmostragem=1000
tempoDeAmostragem=numDeAmostras/freqDeAmostragem
sinal=cos(  ( (2*pi*100) / freqDeAmostragem ) * numDeAmostras  ) + cos(  ( (2*pi*300) / freqDeAmostragem ) * numDeAmostras  )%sinal a ser filtrado. Uma soma de cossenos com frequencia 100Hz e 300Hz.
%}

%plotando o sinal de entrada {
subplot(2,2,1)
plot(tempoDeAmostragem,sinal,'blue')
grid
%}



%calculando a FFT do sinal de entrada{
resolucaoDaFFT=2^(nextpow2(size(sinal,2)))
HeSinal=fft(sinal,resolucaoDaFFT)%m�dulo da transformada de fourier do sinal de entrada.
%}

%plotando a FFT do sinal de entrada{
ModHeSinal=abs(HeSinal)
eixoEmFreq=linspace(0,freqDeAmostragem/2, (size(HeSinal,2)/2) )% gerando eixo de frequencia para exibi��o da FFT
subplot(2,2,2)
% plot(HzSinal,'blue')%plotando a FFT do sinal de entrada.
% plot( (HzSinal(1:floor(size(HzSinal,2)/2))) / max(HzSinal) , 'blue')%plotando a FFT do sinal de entrada.
plot(eixoEmFreq , (ModHeSinal(1:floor(size(ModHeSinal,2)/2))) / (size(ModHeSinal,2)/2), 'blue')%plotando a FFT do sinal de entrada.
grid
%}

%gerando um filtro passa baixa que corta 0.4 acima da amostragem{
meuFiltro = fir1(25,0.4,'low')%gerando um filtro passa baixa que corta 0.4 acima da amostragem.
[respostaEmFreq,w]=freqz(meuFiltro,1,size(eixoEmFreq,2),freqDeAmostragem)
%}

%plotando a resposta em frequencia do filtro{
hold on
subplot(2,2,2)
plot(eixoEmFreq , abs(respostaEmFreq)/max(abs(respostaEmFreq)),'magenta')%plotando a resposta em frequencia em cima da FFT.
%}

%filtrando o sinal de entrada{
sinalFiltrado=filter(meuFiltro,1,sinal)%filtrando do sinal com o filtro gerado e armazenado em 'meuFiltro'
subplot(2,2,3)
plot(tempoDeAmostragem,sinalFiltrado,'red')
grid
%}

%plotando a FFT do sinal filtrado{
HeSinalFiltrado=fft(sinalFiltrado,resolucaoDaFFT)
ModHeSinalFiltrado=abs(HeSinalFiltrado)
subplot(2,2,4)
plot(eixoEmFreq ,  ModHeSinalFiltrado(1:floor(size(ModHeSinalFiltrado,2)/2)) / ((size(ModHeSinalFiltrado,2))/2)  ,'red')%plotando a FFT do sinal j� filtrado
grid
%}
