clear
clc
numDeAmostras=[0:100]
freqDeAmostragem=1000
sinal=cos(((2*%pi*100)/1000)*numDeAmostras)+cos(((2*%pi*300)/1000)*numDeAmostras)//sinal a ser filtrado. Uma soma de cossenos com frequencia 100Hz e 300Hz.
subplot(2,2,1)
plot(sinal,'blue')
xgrid
HzSinal=abs(fft(sinal,1024))//transformada de fourier do sinal a ser filtrado.
subplot(2,2,2)
plot((HzSinal(1:floor(size(HzSinal,2)/2)))/((size(HzSinal,2))/2),'blue')//plotando a FFT do sinal a filtrado
xgrid
//meuFiltro = fir1(25,0.4,'low')%gerando um filtro passa baixa que corta 0.4 acima da amostragem.
//sinalFiltrado=filter(meuFiltro,1,sinal)%filtrando do sinal com o filtro gerado e armazenado em 'meuFiltro'
//[respostaEmFreq,w]=freqz(meuFiltro,1,50,freqDeAmostragem)
//hold on
//plot(abs(respostaEmFreq)/max(abs(respostaEmFreq)))
//subplot(2,2,1)
//hold on
//plot(sinalFiltrado,'red')
//grid
//HzSinalFiltrado=abs(fft(sinalFiltrado))
//subplot(2,2,2)
//hold on
//plot(HzSinalFiltrado(1:floor(size(HzSinalFiltrado,2)/2))/((size(HzSinalFiltrado,2))/2),'red')%plotando a FFT do sinal já filtrado
//grid
//% meuFiltroIIRbutterworth=
//% meuFiltroIIRcheby1=
//% meuFiltroIIRcheby2=