clear
clc
%ex2: Plota a resposta em frequ�ncia de um filtro dados os coeficientes do
%numerador e do denominador da fun��o de transfer�ncia.
HzNumerador=[1,4,6,4,1]
HzDenominador=[1,0,0.486,0,0.0177]
[H,w]=freqz(HzNumerador,HzDenominador)%num de pontos padrao = 512
plot(w,abs(H))%resposta em frequencia, em fun��o de w (omega).