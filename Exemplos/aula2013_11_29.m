%limpando as vari�veis e a tela{
clear
clc
%}
%obtendo sinal de entrada do arduivo 'alert.wav'{
[meuAudio,freqDeAmostragem,Nbits]=wavread('alert.wav');
meuAudio=meuAudio';
tempoDeAmostragem=[0:1:size(meuAudio,2)-1]/freqDeAmostragem;
%}

%plotando o sinal de entrada {
subplot(2,2,1);
plot(tempoDeAmostragem,meuAudio,'blue');
grid;
%}

%calculando a FFT do sinal de entrada{
resolucaoDaFFT=2^(nextpow2(size(meuAudio,2)));
HeMeuAudio=fft(meuAudio,resolucaoDaFFT);%m�dulo da transformada de fourier do sinal de entrada.
%}

%plotando a FFT do sinal de entrada{
ModHeSinal=abs(HeMeuAudio);
eixoEmFreq=linspace(0,freqDeAmostragem/2, (size(HeMeuAudio,2)/2) );% gerando eixo de frequencia para exibi��o da FFT
subplot(2,2,2);
plot(eixoEmFreq , (ModHeSinal(1:floor(size(HeMeuAudio,2)/2))) / (size(HeMeuAudio,2)/2), 'blue');%plotando a FFT do sinal de entrada.
grid
%}

%gerando um filtro passa baixa que corta 0.4 acima da amostragem{
[numeradorDofiltro,denominadorDoFiltro] = butter(15,(2*1200/freqDeAmostragem),'high');%gerando um filtro passa baixa que corta 0.4 acima da amostragem.
[respostaEmFreq,w]=freqz(numeradorDofiltro,denominadorDoFiltro,size(eixoEmFreq,2),freqDeAmostragem);
%}

%plotando a resposta em frequencia do filtro{
hold on
subplot(2,2,2)
picoDoSpectro=max((ModHeSinal(1:floor(size(HeMeuAudio,2)/2))) / (size(HeMeuAudio,2)/2))
plot(eixoEmFreq , abs(respostaEmFreq)*picoDoSpectro,'magenta')%plotando a resposta em frequencia em cima da FFT.
%}

%filtrando o sinal de entrada{
sinalFiltrado=filter(numeradorDofiltro,denominadorDoFiltro,meuAudio)%filtrando do sinal com o filtro gerado e armazenado em 'meuFiltro'
subplot(2,2,3)
plot(tempoDeAmostragem,sinalFiltrado,'red')
grid
%}

%plotando a FFT do sinal filtrado{
HeSinalFiltrado=fft(sinalFiltrado,resolucaoDaFFT)
ModHeSinalFiltrado=abs(HeSinalFiltrado)
subplot(2,2,4)
plot(eixoEmFreq ,  ModHeSinalFiltrado(1:floor(size(ModHeSinalFiltrado,2)/2)) / ((size(ModHeSinalFiltrado,2))/2)  ,'red')%plotando a FFT do sinal j� filtrado
grid
subplot(2,2,4)
hold on
plot(eixoEmFreq , abs(respostaEmFreq)*picoDoSpectro,'magenta')%plotando a resposta em frequencia em cima da FFT.
%}

wavwrite(sinalFiltrado,freqDeAmostragem,Nbits,'filteredAlert.wav')