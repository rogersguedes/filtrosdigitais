%limpando as vari�veis e a tela{
clear%limpa todas as vari�veis.
clc%limpa a tela.
close all%fecha todas as janelas.
%}

%obtendo sinal de entrada do arduivo 'fala_ruido2.wav'{
[meuAudio,freqDeAmostragem,Nbits]=wavread('fala_ruido2.wav');%importa o arquivo de audio e coloca as amostras de audio no vetor meuAudio, a frequ�ncia de amostragem em freqDeAmostragem e a resolu��o a amostra em Nbits.
meuAudio=meuAudio';%transp�e a matriz das amostras de audio. Odeio matriz coluna ^^.
tempoDeAmostragem=[0:1:size(meuAudio,2)-1]/freqDeAmostragem;%gerando vetor com o tempo do audio, para usar na hora plotar os sinal de audio.
%}

%plotando o sinal de entrada {
subplot(2,2,1);%abre uma nova �rea de pltagem dentro de uma janela.
plot(tempoDeAmostragem,meuAudio,'blue');%plota o sinal de audio de entrada.
xlabel('Tempo (s)');%coloca a legenda do gr�fico.
grid;%liga as linhas de grade do gr�fico.
%}

%calculando a FFT do sinal de entrada{
resolucaoDaFFT=2^(nextpow2(size(meuAudio,2)));%calcula a resolu��o para a FFT que seja uma potencia de 2 mais pr�xima do n�mero de amostras do sinal de entrada.
HeMeuAudio=fft(meuAudio,resolucaoDaFFT);%calcula a FFT do Sinal de Entrada.
%}

%plotando a FFT do sinal de entrada{
ModHeSinal=abs(HeMeuAudio);%calcula o m�dulo da transformada de fourier do sinal de entrada.
eixoEmFreq=linspace(0,freqDeAmostragem/2, (size(HeMeuAudio,2)/2) );% gera o eixo de frequencias para exibi��o da FFT
subplot(2,2,2);%abre uma nova �rea de pltagem dentro de uma janela.
plot(eixoEmFreq , (ModHeSinal(1:floor(size(HeMeuAudio,2)/2))) / (size(HeMeuAudio,2)/2), 'blue');%plota a FFT do sinal de entrada.
xlabel('Frequencia (Hz)');%coloca a legenda do gr�fico.
grid;%liga as linhas de grade do gr�fico.
%}

%gerando um filtro notch que corta 550Hz e 1750 Hz
raioDoNotch = 0.99;%radio do denominador do Filtro Notch, quando mais pr�ximo de 1 mais estreita � a Resposta em Frequ�ncia

omegaNotchFreq1 = (2*pi*550)/freqDeAmostragem;%Primeira Frequ�ncia Notch discreta
numeradorDofiltroFreq1=[1,-2*cos(omegaNotchFreq1),1];%Primeiro Numerador do Filtro Notch com sua devida Frequ�ncia Notch 
denominadorDoFiltroFreq1=[1,-2*raioDoNotch*cos(omegaNotchFreq1),raioDoNotch^2];%Primeiro Denominador do Filtro Notch com sua devida Frequ�ncia Notch 

omegaNotchFreq2=(2*pi*1750)/freqDeAmostragem;%Segunda Frequ�ncia Notch discreta
numeradorDofiltroFreq2=[1,-2*cos(omegaNotchFreq2),1];%Segundo Numerador do Filtro Notch com sua devida Frequ�ncia Notch 
denominadorDoFiltroFreq2=[1,-2*raioDoNotch*cos(omegaNotchFreq2),raioDoNotch^2];%Segundo Denominador do Filtro Notch com sua devida Frequ�ncia Notch 

numeradorDofiltroFinal=conv(numeradorDofiltroFreq1,numeradorDofiltroFreq2);%usando conolu��o para multiplicar os coeficientes. Obrigado Ricardo Rodrigues!
denominadorDoFiltroFinal=conv(denominadorDoFiltroFreq1,denominadorDoFiltroFreq2);%usando conolu��o para multiplicar os coeficientes. Obrigado Ricardo Rodrigues!

[respostaEmFreq,w]=freqz(numeradorDofiltroFinal,denominadorDoFiltroFinal,size(eixoEmFreq,2),freqDeAmostragem);%obtendo a Resposta em Frequ�ncia do duplo Filtro Notch
%}

%plotando a resposta em frequencia do filtro{
hold on;%for�a um gr�fico ser plotado em cima do outro.
subplot(2,2,2)%abre uma nova �rea de pltagem dentro de uma janela.
picoDoSpectro=max((ModHeSinal(1:floor(size(HeMeuAudio,2)/2))) / (size(HeMeuAudio,2)/2));%calcula o m�ximo valor da FFT do sinal filtrado para plotar a Resposta do Filtro sobre o mesmo gr�fico da FFT do sinal de entrada. Apenas para quest�o de visualiza��o.
plot(eixoEmFreq , abs(respostaEmFreq)*picoDoSpectro,'magenta')%plotando a resposta em frequencia em cima da FFT.
%}

%filtrando o sinal de entrada{
sinalFiltrado=filter(numeradorDofiltroFinal,denominadorDoFiltroFinal,meuAudio);%filtra o sinal com o filtro gerado e armazenado em 'sinalFiltrado'.
%}

%Plotando o sinal filtrado{
subplot(2,2,3);%abre uma nova �rea de pltagem dentro de uma janela.
plot(tempoDeAmostragem,sinalFiltrado,'red');%plotando o Sinal Filtrado.
xlabel('Tempo (s)');%coloca a legenda do gr�fico.
grid;%liga as linhas de grade do gr�fico.
%}

%plotando a FFT do sinal filtrado{
HeSinalFiltrado=fft(sinalFiltrado,resolucaoDaFFT);%calcula a FFT do Sinal Filtrado.
ModHeSinalFiltrado=abs(HeSinalFiltrado);%calcula o m�dulo da FFT do Sinal Filtrado.
subplot(2,2,4)%abre uma nova �rea de pltagem dentro de uma janela.
plot(eixoEmFreq ,  ModHeSinalFiltrado(1:floor(size(ModHeSinalFiltrado,2)/2)) / ((size(ModHeSinalFiltrado,2))/2)  ,'red')%plotando a FFT do Sinal Filtrado.
xlabel('Frequencia (Hz)');%coloca a legenda do gr�fico.
grid;%liga as linhas de grade do gr�fico.
%}

%plotando a resposta em frequencia do filtro{
hold on%for�a um gr�fico ser plotado em cima do outro.
subplot(2,2,4)%abre uma nova �rea de pltagem dentro de uma janela.
plot(eixoEmFreq , abs(respostaEmFreq)*picoDoSpectro,'magenta')%plotando a resposta em frequencia em cima da FFT do Sinal Filtrado.
%}

% C�lculo da resposta impulsiva do Filtro{
tamanhoDaRespostaImpulsiva=700;%Usei esse valor porque ap�s isso a resposta estabiliza.
impulso=zeros(1,tamanhoDaRespostaImpulsiva);%Gera uma matriz 1x'tamanhoDaRespostaImpulsiva' preenchida com zeros.
impulso(1,1)=1;%coloca 1 no primeiro para criar o sinal 'delta' do impulso unit�rio.
respostaImpulsiva=filter(numeradorDofiltroFinal,denominadorDoFiltroFinal,impulso);%filtra o impulso e obtem resposta impulsiva.
%}

%Plotando a resposta impulsiva{
tempoDaRespostaImpulsiva=[0:1:tamanhoDaRespostaImpulsiva-1]/freqDeAmostragem;%gerando vetor com o tempo da resposta impulsiva, para usar na hora plotar a resposta impulsiva.
figure%cria uma nova janela para plotagem de gr�ficos.
plot(tempoDaRespostaImpulsiva , respostaImpulsiva)%plota a resposta impulsiva em fun��o do tempo.
xlabel('Tempo (s)');%coloca a legenda do gr�fico.
grid;%liga as linhas de grade do gr�fico.
%}

%Gravando o audio filtrado em um arquivo{
wavwrite(sinalFiltrado,freqDeAmostragem,Nbits,'fala_ruido2_after_double_notch.wav')%grava Sinal Filtrado em um arquivo WAV novo usando a mesma Frequ�ncia De Amostragem e Resolu��o de bits.
%}